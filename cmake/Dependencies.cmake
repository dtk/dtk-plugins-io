## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

set(dtkIo_HDF5_LIBS "")

find_package(HDF5 COMPONENTS C)

if(HDF5_FOUND)

if(DTK_BUILD_DISTRIBUTED AND HDF5_IS_PARALLEL)
  find_package(MPI REQUIRED)
  mark_as_advanced(MPI_EXTRA_LIBRARY)
  mark_as_advanced(MPI_LIBRARY)

  include_directories(SYSTEM ${MPI_INCLUDE_PATH})
  set(COMPILE_FLAGS ${COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})

  list(APPEND dtkIo_HDF5_LIBS ${MPI_LIBRARIES})

  set(CMAKE_REQUIRED_INCLUDES "${MPI_INCLUDE_PATH}")
  set(CMAKE_REQUIRED_LIBRARIES "${MPI_LIBRARIES}")

  add_definitions(-DADD_PHDF5)
endif()

include_directories(SYSTEM ${HDF5_INCLUDE_DIRS})

list(APPEND dtkIo_HDF5_LIBS ${HDF5_C_LIBRARIES})

mark_as_advanced(HDF5_DIR)

endif(HDF5_FOUND)

mark_as_advanced(CLEAR HDF5_hdf5_LIBRARY_RELEASE)
mark_as_advanced(CLEAR HDF5_IS_PARALLEL)


######################################################################
### Dependencies.cmake ends here
