// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "pHdf5IoDataModel.h"
#include "pHdf5IoDataModelPlugin.h"

// ///////////////////////////////////////////////////////////////////
// pHdf5IoDataModelPlugin
// ///////////////////////////////////////////////////////////////////

void pHdf5IoDataModelPlugin::initialize(void)
{
    dtkIo::dataModel::pluginFactory().record("pHdf5IoDataModel", pHdf5IoDataModelCreator);
}

void pHdf5IoDataModelPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(pHdf5IoDataModel)

//
// pHdf5IoDataModelPlugin.cpp ends here
