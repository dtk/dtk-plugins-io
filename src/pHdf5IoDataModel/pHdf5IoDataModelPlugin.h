// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkIo>

class pHdf5IoDataModelPlugin : public dtkIoDataModelPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkIoDataModelPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.pHdf5IoDataModelPlugin" FILE "pHdf5IoDataModelPlugin.json")

public:
     pHdf5IoDataModelPlugin(void) {}
    ~pHdf5IoDataModelPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// pHdf5IoDataModelPlugin.h ends here
