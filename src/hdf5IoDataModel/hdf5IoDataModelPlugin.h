// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>
#include <dtkIo>

class hdf5IoDataModelPlugin : public dtkIoDataModelPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkIoDataModelPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.hdf5IoDataModelPlugin" FILE "hdf5IoDataModelPlugin.json")

public:
     hdf5IoDataModelPlugin(void) {}
    ~hdf5IoDataModelPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// hdf5IoDataModelPlugin.h ends here
