// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <cstdlib>
#include <ctime>

#include "dtkPluginsIoTestConfig.h"

#include "mpi.h"

#include <QFileInfo>

#include <dtkDistributed>
#include <dtkIo>

#include <unistd.h>



#define DTK_IO_PHDF5_INIT_PLUGIN \
        dtkIoDataModel *data_model = dtkIo::dataModel::pluginFactory().create("pHdf5IoDataModel"); \
        data_model->setCommunicator(dtkDistributed::app()->communicator()); \
        int rank = dtkDistributed::app()->communicator()->rank(); \
        int size = dtkDistributed::app()->communicator()->size();

static bool fileExists(QString file) {
    QFileInfo checkFile(file);
    // check if file exists and if yes: Is it really a file and no directory?
    if (checkFile.exists() && checkFile.isFile()) {
        return true;
    } else {
        return false;
    }
}

#define NPROC 4
#define NB_POINTS 64
#define HYSL_NX 8
#define HYSL_NY 8
#define WRBYCOOR_SIZE 10000
#define PRIME_NUMBER 9887 // prime number just lower WRBYCOOR_SIZE

class testCreateRunnable : public QRunnable
{
public:
    void run(void) 
    {
        DTK_IO_PHDF5_INIT_PLUGIN
        QString file_name = "fileCreationTestP.h5";
        data_model->fileOpen(file_name, dtkIoDataModel::Trunc);
        data_model->fileClose();

        DTK_DISTRIBUTED_BEGIN_GLOBAL
        QVERIFY(fileExists(file_name));
        DTK_DISTRIBUTED_END_GLOBAL

        delete data_model;
    }
    
};


class testReadRunnable : public QRunnable
{
public:
    void run(void)
    {
        DTK_IO_PHDF5_INIT_PLUGIN
        QString file_name = DTK_PLUGINS_IO_EXAMPLE_PATH + "/../example.h5";
        data_model->fileOpen(file_name, dtkIoDataModel::ReadOnly);
        int dset_data[6][15];
        QString dataset_name = "/dset";
        data_model->read(dataset_name, dtkIoDataModel::Int, dset_data);
        data_model->fileClose();

        QCOMPARE(dset_data[0][0], 1);
        QCOMPARE(dset_data[0][1], 2);
        QCOMPARE(dset_data[1][0], 16);
        QCOMPARE(dset_data[5][0], 76);
        QCOMPARE(dset_data[5][14], 90);

        delete data_model;
    }
};


class testReadByCoordRunnable : public QRunnable
{
public:
    void run(void)
    {
        DTK_IO_PHDF5_INIT_PLUGIN
        QString file_name = DTK_PLUGINS_IO_EXAMPLE_PATH + "/../example1000x1000.h5";
        data_model->fileOpen(file_name, dtkIoDataModel::ReadOnly);

        std::srand(std::time(0)); //use current time as seed for random generator

        double dset_data[NB_POINTS];
        quint64 points_coord[NB_POINTS][2];
        for(int i=0; i< NB_POINTS; ++i) {
            points_coord[i][0] = 999*(std::rand()/(1.0*RAND_MAX));
            points_coord[i][1] = 999*(std::rand()/(1.0*RAND_MAX)); 
        }
                    
        QString dataset_name = "/mediumarray";
        data_model->readByCoord(dataset_name, dtkIoDataModel::Double, NB_POINTS, &points_coord[0][0], &dset_data[0]);
        data_model->fileClose();

        double correct_value;
        for(int i=0; i<NB_POINTS; ++i)
        {
            correct_value = points_coord[i][0]*1000+points_coord[i][1];
            if(points_coord[i][0]%2)
                correct_value = -correct_value;
            QCOMPARE(dset_data[i], correct_value);
        }

        delete data_model;
    }
};

class testWriteHyperslabRowRunnable : public QRunnable
{
public:
    void run(void)
    {
        DTK_IO_PHDF5_INIT_PLUGIN
        QString file_name = "testParalWriteHyperslabRow.h5";
        QString dataset_name = "/hyperslabRow";
        //first delete the file if it already exist
        DTK_DISTRIBUTED_BEGIN_GLOBAL
        if(fileExists(file_name)) {
            //delete the file
            QFile::remove(file_name);
        }
        DTK_DISTRIBUTED_END_GLOBAL
        
        // next write the Hyperslab
        data_model->fileOpen(file_name, dtkIoDataModel::Trunc);

        //call write with no values to create the dataset
        quint64 shape_global[2] = { HYSL_NX, HYSL_NY};
        data_model->write(dataset_name, dtkIoDataModel::Double, 2, shape_global);

        //now prepare the data and write the hyperslab
        quint64 count_local[2] = {HYSL_NX/NPROC, HYSL_NY};
        int local_size = (HYSL_NX/NPROC)* HYSL_NY;
        quint64 offset[2];
        offset[0] = rank*count_local[0];
        offset[1] = 0;
        quint64 block[2] = {1,1};
        quint64 stride[2] = {1,1};
        
        double *data_local = new double[local_size];
        for(int i=0; i<local_size; ++i)
        {
            data_local[i] = rank*10.0;
        }

        data_model->writeHyperslab(dataset_name, dtkIoDataModel::Double, offset, stride, count_local, block, count_local, data_local);
        data_model->fileClose();

        //lastly, check written data
        double r_data[HYSL_NX*HYSL_NY] ;
        data_model->fileOpen(file_name, dtkIoDataModel::ReadOnly);
        data_model->read(dataset_name, dtkIoDataModel::Double, r_data);
        data_model->fileClose();

        DTK_DISTRIBUTED_BEGIN_GLOBAL
        for(int proc=0; proc<NPROC; ++proc)
        {
            for(int i=0; i<count_local[0]*count_local[1]; ++i)
            {
                QCOMPARE(r_data[i + proc*count_local[0]*HYSL_NY], 10.0*proc);
            }
        }
        DTK_DISTRIBUTED_END_GLOBAL

        delete[] data_local;
        delete data_model;
    }
};

// p being a prime number, 
//quadratic residue of x = ((x*x)mod p) is unique as long as 2x<p  .  so for 
class testWriteByCoordRunnable : public QRunnable
{
public:
    void run(void)
    {
        DTK_IO_PHDF5_INIT_PLUGIN
        QString file_name = "testParalWriteByCoord.h5";
        QString dataset_name = "/dataByCoord";
        //first delete the file if it already exist
        DTK_DISTRIBUTED_BEGIN_GLOBAL
        if(fileExists(file_name)) {
            //delete the file
            QFile::remove(file_name);
        }
        DTK_DISTRIBUTED_END_GLOBAL
            
        //security check
        if(NB_POINTS*NPROC*2 >= PRIME_NUMBER) {
            qDebug() << "WARNING NPROC*NB_POINTS too big compared to PRIME_NUMBER ! you may have problems ;) ";
        }
        
        // next write the data
        data_model->fileOpen(file_name, dtkIoDataModel::Trunc);

        //call write with no values to create the dataset
        //max 8 process
        quint64 shape_global[1] = {WRBYCOOR_SIZE};
        data_model->write(dataset_name, dtkIoDataModel::Double, 1, shape_global);

        quint64 points_coord[NB_POINTS];
        double data_local[NB_POINTS];
    
        for(int i=0; i< NB_POINTS; ++i) {
            points_coord[i] = ((i + NB_POINTS*rank ) *  (i + NB_POINTS*rank ) ) % PRIME_NUMBER;
            data_local[i] = (1.45*(rank+1))*i; 
        }

        data_model->writeByCoord(dataset_name, dtkIoDataModel::Double, NB_POINTS, points_coord, data_local);
        data_model->fileClose();

        //lastly, check written data
        double r_data[WRBYCOOR_SIZE];
        data_model->fileOpen(file_name, dtkIoDataModel::ReadOnly);
        data_model->read(dataset_name, dtkIoDataModel::Double, r_data);
        data_model->fileClose();


        for(int i=0; i<NB_POINTS; ++i)
        {
            double correct_value = (1.45*(rank+1))*i;
            QCOMPARE(r_data[points_coord[i]] , correct_value);
        }

        delete data_model;
    }
};

class testWriteByCoordGroupRunnable : public QRunnable
{
public:
    void run(void)
    {
        DTK_IO_PHDF5_INIT_PLUGIN
        QString file_name = "testParalWriteByCoordGroup.h5";
        QString dataset_name = "/thegroup/dataByCoord";
        //first delete the file if it already exist
        DTK_DISTRIBUTED_BEGIN_GLOBAL
        if(fileExists(file_name)) {
            //delete the file
            QFile::remove(file_name);
        }
        DTK_DISTRIBUTED_END_GLOBAL
        
        // next write the data
        data_model->fileOpen(file_name, dtkIoDataModel::Trunc);

        //call write with no values to create the dataset
        //max 8 process
        quint64 shape_global[1] = {WRBYCOOR_SIZE};
        data_model->write(dataset_name, dtkIoDataModel::Double, 1, shape_global);

        quint64 points_coord[NB_POINTS];
        double data_local[NB_POINTS];
    
        for(int i=0; i< NB_POINTS; ++i) {
            points_coord[i] = ((i + NB_POINTS*rank ) *  (i + NB_POINTS*rank ) ) % PRIME_NUMBER;
            data_local[i] = (1.45*(rank+1))*i; 
        }

        data_model->writeByCoord(dataset_name, dtkIoDataModel::Double, NB_POINTS, points_coord, data_local);
        data_model->fileClose();

        //lastly, check written data
        double r_data[WRBYCOOR_SIZE];
        data_model->fileOpen(file_name, dtkIoDataModel::ReadOnly);
        data_model->read(dataset_name, dtkIoDataModel::Double, r_data);
        data_model->fileClose();


        for(int i=0; i<NB_POINTS; ++i)
        {
            double correct_value = (1.45*(rank+1))*i;
            QCOMPARE(r_data[points_coord[i]] , correct_value);
        }

        delete data_model;
        
    }
};

// 
// pHdf5IoDataModelRunnable.h ends here
