// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

class pHdf5IoDataModelTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCreate(void);
    void testRead(void);
    void testReadByCoord(void);
    void testWriteHyperslabRow(void);
    void testWriteByCoord(void);
    void testWriteByCoordGroup(void);    
private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


//
// pHdf5IoDataModelTest.h ends here
