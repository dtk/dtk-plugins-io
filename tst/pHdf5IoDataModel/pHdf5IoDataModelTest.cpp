// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "pHdf5IoDataModelTest.h"
#include "pHdf5IoDataModelRunnable.h"

#include <dtkDistributed/dtkDistributedPolicy>

#include <QFileInfo>
#include <dtkIo>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void pHdf5IoDataModelTestCase::initTestCase(void)
{
    dtkDistributedSettings dsettings;
    dsettings.beginGroup("communicator");
    dtkDistributed::communicator::pluginManager().initialize(dsettings.value("plugins").toString());
    dsettings.endGroup();
    dtkDistributed::policy()->setType("mpi3");

    dtkIoSettings settings;
    settings.beginGroup("io");
    dtkIo::dataModel::initialize(settings.value("plugins").toString());
    settings.endGroup();

    for (int i = 1; i < NPROC; ++i) {
        dtkDistributed::policy()->addHost("localhost");
    }

    dtkDistributed::spawn();
}

void pHdf5IoDataModelTestCase::init(void)
{
}

void pHdf5IoDataModelTestCase::testCreate(void)
{
    QRunnable *test = new testCreateRunnable();
    dtkDistributed::exec(test);
    delete test;
    
}

void pHdf5IoDataModelTestCase::testRead(void)
{
    QRunnable *test = new testReadRunnable();
    dtkDistributed::exec(test);
    delete test;    
}

void pHdf5IoDataModelTestCase::testReadByCoord(void)
{
    QRunnable *test = new testReadByCoordRunnable();
    dtkDistributed::exec(test);
    delete test;    
}

void pHdf5IoDataModelTestCase::testWriteHyperslabRow(void)
{
    QRunnable *test = new testWriteHyperslabRowRunnable();
    dtkDistributed::exec(test);
    delete test;    
}

void pHdf5IoDataModelTestCase::testWriteByCoord(void)
{
    QRunnable *test = new testWriteByCoordRunnable();
    dtkDistributed::exec(test);
    delete test;    
}

void pHdf5IoDataModelTestCase::testWriteByCoordGroup(void)
{
    QRunnable *test = new testWriteByCoordGroupRunnable();
    dtkDistributed::exec(test);
    delete test;    
}

void pHdf5IoDataModelTestCase::cleanupTestCase(void)
{
    dtkDistributed::unspawn();
    dtkDistributed::communicator::pluginManager().uninitialize();
    //  dtkIo::dataModel::uninitialize();
}

void pHdf5IoDataModelTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(pHdf5IoDataModelTest, pHdf5IoDataModelTestCase)

//
// pHdf5IoDataModelTest.cpp ends here
